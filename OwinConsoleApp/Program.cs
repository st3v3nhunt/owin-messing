﻿namespace OwinConsoleApp
{
  using System;

  using Microsoft.Owin.Hosting;

  using Owin;

  class Program
  {
    static void Main(string[] args)
    {
      using (WebApp.Start<Startup>("http://localhost:8080/"))
      {
        Console.WriteLine("Started...");
        Console.ReadKey();
        Console.WriteLine("Stopping...");
      }
    }

    // Named 'Startup' by OWIN convention
    public class Startup
    {
      public void Configuration(IAppBuilder app)
      {
        app.Run(
          ctx =>
            {
              return ctx.Response.WriteAsync("Hello World!\n");
            });
      }
    }
  }
}
